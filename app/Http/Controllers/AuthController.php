<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AuthRequest;
use App\Http\Controllers\Controller;
use Auth;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(AuthRequest $request)
    {
        $input = $request->input();

        $user = [
            'login' => $input['login'],
            'password' => $input['password']
        ];

        if (Auth::attempt($user))
        {
            return redirect()->route('inicio');
        }

        return redirect('/');
    }

    public function getLogout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
