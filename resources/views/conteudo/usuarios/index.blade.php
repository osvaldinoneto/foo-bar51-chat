
@extends('layout.default')

@section('content')

<h1>Formulário</h1>

<a href="{!! URL::route('usuario.create') !!}" class="btn btn-primary btn-xs">Criar novo</a>

@if ($usuarios)
<table class="table">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Login</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($usuarios as $item)
        <tr>
            <td>{{ $item->nome }}</td>
            <td>{{ $item->login }}</td>
            <td><a href="{!! URL::route('usuario.edit', [$item]) !!}">Editar</a></td>
            <td>
                {!! Form::open(['route'=>['usuario.destroy', $item],'method'=>'delete']) !!}
                <button type="submit" class="btn btn-danger btn-xs">Excluir</button>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div class="well">
    Não há itens a serem exibidos.
</div>
@endif

@endsection
