
@extends('layout.default')

@section('content')

<h1>Formulário</h1>

@if (isset($usuario))
{!! Form::open(['route'=>['usuario.update',$usuario],'method'=>'put']) !!}
@else
{!! Form::open(['route'=>'usuario.store','method'=>'post']) !!}
@endif

<div class="row-fluid">
    <div class="col-lg-6">
        {!! Form::label('nome', 'Nome') !!}
        {!! Form::text('nome', (isset($usuario) ? $usuario->nome : old('nome')), ['class'=>'form-control']) !!}
    </div>
</div>
<div class="row-fluid">
    <div class="col-lg-6">
        {!! Form::label('login', 'Login') !!}
        {!! Form::text('login', (isset($usuario) ? $usuario->login : old('login')), ['class'=>'form-control']) !!}
    </div>
</div>
<div class="row-fluid">
    <div class="col-lg-6">
        {!! Form::label('password', 'Senha') !!}
        {!! Form::password('password', ['class'=>'form-control']) !!}
    </div>
</div>
<div class="row-fluid">
    <div class="col-lg-6">
        {!! Form::label('password_confirmation', 'Confirme a senha') !!}
        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
    </div>
</div>

<div class="row-fluid pull-right">
    <div class="col-lg-12">
        <a href="{!! URL::route('inicio') !!}" class="btn btn-default">Voltar</a>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
</div>

{!! Form::close() !!}

@endsection
